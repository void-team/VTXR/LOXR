# Lightweight OpenXR Runtime

Lightweight OpenXR Runtime is just that, a lightweight, developer friendly version of an OpenXR runtime. It's purpose is to serve the bare essentials that an OpenXR runtime needs to serve in order to provide an easily distributable development platform for use with ACTUAL, FULL OpenXR Runtimes. It was built with one goal in mind, to aid development of Games and other lightweight, distributable applications. If your use of this project does not fall under these two catergories, you're probably looking for something else.




# References

https://www.khronos.org/openxr/


https://registry.khronos.org/OpenXR/#apispecs


https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html

